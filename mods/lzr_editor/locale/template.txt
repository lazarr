# textdomain: lzr_editor
Start or exit level editor=
[ enter | exit ]=
Already in level editor!=
Not in level editor!=
Set level size for editor (warning: clears level contents!)=
Not in editor mode!=
Too large!=
Too small!=
Save current level=
Level saved to @1.=
Error writing level file!=
Load level=
Level file does not exist!=
Level loaded.=
Error reading level file!=
