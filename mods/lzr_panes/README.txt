Lazarr! mod: lzr_panes
======================
A simplified version of xpanes.

See license.txt for license information.

Original authors of source code
-------------------------------
xpanes mod originally by xyz (MIT)
BlockMen (MIT)
sofar (MIT)
Various Minetest developers and contributors (MIT)

Transformation to Lazarr! version of this mod: Wuzzy (MIT)

Authors of media (textures)
---------------------------
xyz (CC BY-SA 3.0):
  All textures not mentioned below.

Gambit (CC BY-SA 3.0):
  xpanes_bar.png

paramat (CC BY-SA 3.0):
  xpanes_bar_top.png

Krock (CC0 1.0):
  xpanes_edge.png
