lzr_laser = {}

dofile(minetest.get_modpath("lzr_laser").."/util.lua")
dofile(minetest.get_modpath("lzr_laser").."/laser.lua")
dofile(minetest.get_modpath("lzr_laser").."/blocks.lua")
dofile(minetest.get_modpath("lzr_laser").."/physics.lua")
dofile(minetest.get_modpath("lzr_laser").."/tools.lua")
