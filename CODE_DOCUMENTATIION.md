# Code Documentation for Lazarr!
This file documents the code of this game.
This file is still in WIP.

## Function reference
TODO

## Node special fields reference
This is a list of special fields for node definitions that Lazarr! recognizes:

* `_lzr_active`: If the node has an 'active' counterpart, this contains the
    itemstring of that 'active' counterpart.
    Used by blocks that interact with laser
* `_lzr_inactive`: If the node has an 'inactive' counterpart, this contains the
    itemstring of that 'inactive' counterpart.
    Used by blocks that interact with laser
* `_lzr_on_toggle(pos, node)`: Function is called when a special
    tool is used on this node.
  `pos` is node position and `node` is node table
* `_lzr_unlock(pos, node)`: Function is called on chests to unlock it.
    `pos` is node position and `node` is node table
* `_lzr_send_treasure(pos, node)`: Function is called on open chests
    with treasure to start an animation that "sends" the treasure into
    the sky. Used when a level is completed.
    `pos` is node position and `node` is node table

## Node groups reference
This is a reference of all groups used for nodes.

### Digging groups
* `breakable=1`: Node is breakable with Ultra Pickaxe
* `punchdig=1`: Node breaks when punched
* `takable=1`: Node can be taken by player to be placed somewhere else

### Gameplay groups
* `rotatable`: Node is rotatable by hook item
    * `1`: Always rotatable
    * `2`: Rotatable if takable, or if in editor/menu
    * `3`: Rotatable in editor/menu only
* `laser_destroys`: Node is destroyed when a laser touches it
    * `1`: Immediate destruction
    * `2`: Node catches fire first, then is destroyed.
           `_lzr_active` must contain the name of the burning variant.
           The burning variant in turn must have `_lzr_inactive`
* `flammable=1`: Node catches fire from neighboring burning blocks

### Laser node groups
* `laser=X`: Node is a laser (X=bitmask of active laser axes)
  (not including blocks that *contain* a laser like an active mirror)
* `laser_block=1`: Node interacts with laser

#### Laser blocks
Group rating 1 is for the inactive state, group rating 2 is for the active state.

* `emitter`: Emitter
* `detector`: Detector
* `crystal`: Crystal
* `mirror`: Mirror (normal)
* `transmissive_mirror`: Transmissive Mirror
* `crate`: Crate

### Misc. categorization groups
* `treasure=1`: Treasure
* `teleporter`: Teleporter
    * `1`: Off
    * `2`: On
* `chest`: Chest
    * `1`: closed, unlocked
    * `2`: closed, locked
    * `3`: open, empty
    * `4`: open, with treasure
* `chest_closed=1`: Closed chest
* `chest_open=1`: Open chest
* `chest_open_treasure=1`: Open chest with treasure
* `water=3`: Water
* `liquid=3`: Node is a liquid in a semantic sense
* `pane=1`: Pane (flat window-like block)
* `stair=1`: Stair
* `slab=1`: Slab

## Item groups reference
This is a reference of groups for all items that are not nodes.

* `pickaxe=1`: Pickaxe
* `bucket=1`: Bucket
* `cheat_item=1`: Item is not supposed to be used in normal gameplay (e.g. for editor)
