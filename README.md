# Lazarr!
A laser puzzle game by Wuzzy. Version: 1.3.2

You're a pirate in the search of treasure. Find all shiny gold blocks
in treasure chests to advance to the next level.
To break the lock, you must first solve a laser puzzle by sending lasers
to all detectors. Mirrors and other things will help you.

Originally, this game was hastily made for the Minetest Game Jam 2021 in the
final day of the competition. Version 1.2 was the version that was
rated by the jury and players.
The game has seen a few improvements since then.

Note there aren't a lot of levels at the moment.
(I'm lacking good level ideas)

## How to play
You need Minetest version 5.5.0 (or later) to play this.

Refer to Minetest documentation to learn how to install a game in Minetest.

Once installed, create a world and start. The world contains your game's
progress. You start in the captain's room with a book, use the book
and select a level. Completed levels are in green.

## Level Editor
Lazarr! contains a very crude level editor. It's not great but better than nothing.

See `LEVEL_EDITOR.md` to read how it works.

## Credits/License
See CREDITS.md for credits and license.
